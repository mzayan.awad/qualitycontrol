Django==4.2.8
django-constrainedfilefield==5.0.0
django-phonenumber-field==7.1.0
phonenumbers==8.13.11
Babel==2.12.1
django-countries==7.5.1
python-magic==0.4.27
django-crispy-forms==2.1
crispy-bootstrap4==2023.1
django-addanother==2.2.2
django-tables2==2.7.0
openpyxl==3.0.10
pyexcel_xlsx==0.6.0
django-filter==23.5
django-bootstrap3==23.6
django-select2==8.1.2

